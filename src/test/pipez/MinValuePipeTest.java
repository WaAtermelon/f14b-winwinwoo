package pipez;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;

import static pipez.util.TestUtils.*;
import static pipez.core.Utils.*;

public class MinValuePipeTest {

	@Test
	public void test_positive_integers() throws Exception {
		SimpleBlock block = new SimpleBlock(); 
		block.add(1,2,3,4,5);
		
		MinValuePipe mvp = MinValuePipe.create();
		
		Block b = mvp.transform(block);
		assertThat(b.fields().length, is(1));
		assertThat(dblsOf(b.values()), areCloseTo(1.0));
	}
	
	@Test
	public void test_same() throws Exception {
		SimpleBlock block = new SimpleBlock(); 
		block.add(1,1,3,4,5);
		
		MinValuePipe mvp = MinValuePipe.create();
		
		Block b = mvp.transform(block);
		assertThat(b.fields().length, is(1));
		assertThat(dblsOf(b.values()), areCloseTo(1.0));
	}
	
	@Test
	public void test_zero() throws Exception {
		SimpleBlock block = new SimpleBlock(); 
		block.add(0, 0, 0);
		
		MinValuePipe mvp = MinValuePipe.create();
		
		Block b = mvp.transform(block);
		assertThat(b.fields().length, is(1));
		assertThat(dblsOf(b.values()), areCloseTo(0.0));
	}
	
	@Test
	public void test_negative_integers() throws Exception {
		SimpleBlock block = new SimpleBlock(); 
		block.add(-1, -2, -3, -4, -5);
		
		MinValuePipe mvp = MinValuePipe.create();
		
		Block b = mvp.transform(block);
		assertThat(b.fields().length, is(1));
		assertThat(dblsOf(b.values()), areCloseTo(-5.0));
	}
	
	@Test
	public void test_letters() throws Exception {
		SimpleBlock block = new SimpleBlock(); 
		block.add("a","b","c","d","e");
		
		MinValuePipe mvp = MinValuePipe.create();
		
		Block b = mvp.transform(block);
		assertThat(b.fields().length, is(0));
		
	}
	
	@Test
	public void test_letters_numbers() throws Exception {
		SimpleBlock block = new SimpleBlock(); 
		block.add("1","2","c","4","e");
		
		MinValuePipe mvp = MinValuePipe.create();
		
		Block b = mvp.transform(block);
		assertThat(b.fields().length, is(0));
		
	}
}