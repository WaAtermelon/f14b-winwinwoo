package pipez;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;

public class OddFieldsPipeTest {

	@Test
	public void test_three_columns() throws Exception {
	
		OddFieldsPipe evp = OddFieldsPipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "ABC"); //first column
		sb.add("C2", "def"); //second column
		sb.add("C3", "GHI"); //third column
		
		
		Block tb = evp.transform(sb);
		assertThat(tb.fields().length, is(2));
		assertThat(tb.fields()[0], is("C1"));
		assertThat(tb.fields()[1], is("C3"));
	}
	@Test
	public void test_five_columns() throws Exception {
		
		OddFieldsPipe evp = OddFieldsPipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "ABC"); //first column
		sb.add("C2", "def"); //second column
		sb.add("C3", "GHI"); //third column
		sb.add("C4", "ijk"); //fourth column
		sb.add("C5", "LMN"); //third column

		Block tb = evp.transform(sb);
		assertThat(tb.fields().length, is(3));
		assertThat(tb.fields()[0], is("C1"));
		assertThat(tb.fields()[1], is("C3"));
		assertThat(tb.fields()[2], is("C5"));
	}
	
	@Test
	public void test_twenty_columns() throws Exception {
		
		OddFieldsPipe evp = OddFieldsPipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "ABC"); //first column
		sb.add("C2", "def"); //second column
		sb.add("C3", "GHI"); //third column
		sb.add("C4", "ijk"); //fourth column
		sb.add("C5", "LMN"); //third column
		sb.add("C6", "ABC"); //first column
		sb.add("C7", "def"); //second column
		sb.add("C8", "GHI"); //third column
		sb.add("C9", "ijk"); //fourth column
		sb.add("C10", "LMN"); //third column
		sb.add("C11", "ABC"); //first column
		sb.add("C12", "def"); //second column
		sb.add("C13", "GHI"); //third column
		sb.add("C14", "ijk"); //fourth column
		sb.add("C15", "LMN"); //third column
		sb.add("C16", "ABC"); //first column
		sb.add("C17", "def"); //second column
		sb.add("C18", "GHI"); //third column
		sb.add("C19", "ijk"); //fourth column
		sb.add("C20", "LMN"); //third column

		Block tb = evp.transform(sb);
		assertThat(tb.fields().length, is(10));
		assertThat(tb.fields()[0], is("C1"));
		assertThat(tb.fields()[1], is("C3"));
		assertThat(tb.fields()[2], is("C5"));
		assertThat(tb.fields()[3], is("C7"));
		assertThat(tb.fields()[4], is("C9"));
		assertThat(tb.fields()[5], is("C11"));
		assertThat(tb.fields()[6], is("C13"));
		assertThat(tb.fields()[7], is("C15"));
		assertThat(tb.fields()[8], is("C17"));
		assertThat(tb.fields()[9], is("C19"));
	}
}

