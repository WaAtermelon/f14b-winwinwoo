package pipez;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;
import static pipez.util.TestUtils.*;

public class ValueMatchPipeTest {

	@Test
	public void test_match() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		ValueMatchPipe select = ValueMatchPipe.create("efg");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(4));
		assertThat(b.values(), are("abc", "cde", "efg", "ghi" ));
		
	}
	
	@Test
	public void test_nomatch() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		ValueMatchPipe select = ValueMatchPipe.create("ijk");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(0));
	}
	
	@Test public void test_nomatch_case() {
		
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		ValueMatchPipe select = ValueMatchPipe.create("EFG");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(0));
	}
	
	@Test
	public void test_match_ignorecase() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		ValueMatchPipe select = ValueMatchPipe.createIgnoreCase("EFG");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(4));
		assertThat(b.values(), are("abc", "cde", "efg", "ghi" ));
		
	}
	
	@Test
	public void test_nomatch_ignorecase() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		ValueMatchPipe select = ValueMatchPipe.createIgnoreCase("JKL");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(0));
		
	}
	
	@Test
	public void test_match_inverse() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		ValueMatchPipe select = ValueMatchPipe.createInverse("efg");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(0));
		
		
	}

	@Test
	public void test_nomatch_inverse() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		ValueMatchPipe select = ValueMatchPipe.createInverse("IJK");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(4));
		assertThat(b.values(), are("abc", "cde", "efg", "ghi" ));
	
		}
	
	@Test
	public void test_match_inverse_ignorecase() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		ValueMatchPipe select = ValueMatchPipe.createInverseIgnoreCase("EFG");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(0));
		
	}
	
	@Test
	public void test_nomatch_inverse_ignorecase() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		ValueMatchPipe select = ValueMatchPipe.createInverseIgnoreCase("JKL");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(4));
		assertThat(b.values(), are("abc", "cde", "efg", "ghi" ));
	}
	
	@Test
	public void test_match_numbers() {
		SimpleBlock sb = new SimpleBlock("123", "456", "789", "101" );
		
		ValueMatchPipe select = ValueMatchPipe.create("123");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(4));
		assertThat(b.values(), are("123", "456", "789", "101"));
		
	}
	
	@Test
	public void test_match_special() {
		SimpleBlock sb = new SimpleBlock("*&^", "$%^", "$@^", "!@#" );
		
		ValueMatchPipe select = ValueMatchPipe.create("*&^");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(4));
		assertThat(b.values(), are("*&^", "$%^", "$@^", "!@#"));
		
	}
	
	@Test
	public void test_nomatch_special() {
		SimpleBlock sb = new SimpleBlock("*&^", "$%^", "$@^", "!@#" );
		
		ValueMatchPipe select = ValueMatchPipe.create("*()");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(0));
		
		
	}
	
	
	@Test
	public void test_nomatch_numbers() {
		SimpleBlock sb = new SimpleBlock("123", "456", "789", "101" );
		
		ValueMatchPipe select = ValueMatchPipe.create("678");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(0));
		
		
	}	
	

	
}
