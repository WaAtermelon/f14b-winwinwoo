package pipez;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;
import static pipez.util.TestUtils.*;
import static pipez.core.Utils.*;

public class StatisticsPipeTest {

	@Test
	public void test_mean_integers() {
		SimpleBlock sb = new SimpleBlock();
		sb.add(1, 2, 3, 4, 5);
		
		StatisticsPipe sp = StatisticsPipe.createMean();
		Block b = sp.transform(sb);

		assertThat(b.values().length, is(1));
		assertThat(dblsOf(b.values()), areCloseTo(3.0));
		
	}
	
	@Test
	public void test_mean_decimals() {
		SimpleBlock sb = new SimpleBlock();
		sb.add(1.1, 2.2, 3.3, 4.4, 5.5);
		
		StatisticsPipe sp = StatisticsPipe.createMean();
		Block b = sp.transform(sb);

		assertThat(b.values().length, is(1));
		assertThat(dblsOf(b.values()), areCloseTo(3.3));
		
	}
	
	@Test
	public void test_frequency_integers() {
		SimpleBlock sb = new SimpleBlock();
		sb.add(1, 2, 1, 3, 4, 1);
		
		StatisticsPipe sp = StatisticsPipe.createFrequency(1);
		Block b = sp.transform(sb);

		assertThat(b.values().length, is(1));
		assertThat(dblsOf(b.values()), areCloseTo(3.0));
		
	}
	
	@Test
	public void test_frequency_decimals() {
		SimpleBlock sb = new SimpleBlock();
		sb.add(1, 0.99, 1.01, 3, 4, 1.01);
		
		StatisticsPipe sp = StatisticsPipe.createFrequency(1.01);
		Block b = sp.transform(sb);

		assertThat(b.values().length, is(1));
		assertThat(dblsOf(b.values()), areCloseTo(2.0));
		
	}
	
	@Test
	public void test_median_one_value() {
		SimpleBlock sb = new SimpleBlock();
		sb.add(3);
		
		StatisticsPipe sp = StatisticsPipe.createMedian();
		Block b = sp.transform(sb);

		assertThat(b.values().length, is(1));
		assertThat(dblsOf(b.values()), areCloseTo(3.0));
		
	}
	
	@Test
	public void test_median_odd_integers() {
		SimpleBlock sb = new SimpleBlock();
		sb.add(1, 2, 4, 3, 5);
		
		StatisticsPipe sp = StatisticsPipe.createMedian();
		Block b = sp.transform(sb);

		assertThat(b.values().length, is(1));
		assertThat(dblsOf(b.values()), areCloseTo(3.0));
		
	}
	
	@Test
	public void test_median_even_integers() {
		SimpleBlock sb = new SimpleBlock();
		sb.add(1, 2, 4, 3);
		
		StatisticsPipe sp = StatisticsPipe.createMedian();
		Block b = sp.transform(sb);

		assertThat(b.values().length, is(1));
		assertThat(dblsOf(b.values()), areCloseTo(2.5));
		
	}
	
	@Test
	public void test_mode_integers() {
		SimpleBlock sb = new SimpleBlock();
		sb.add(1, 2, 4, 3, 4, 4, 5, 3, 4, 5);
		
		StatisticsPipe sp = StatisticsPipe.createMode();
		Block b = sp.transform(sb);

		assertThat(b.values().length, is(1));
		assertThat(dblsOf(b.values()), areCloseTo(4.0));
		
	}
	
	@Test
	public void test_mode_decimals() {
		SimpleBlock sb = new SimpleBlock();
		sb.add(1, 2.01, 2, 2, 2, 4.04, 3, 4.04, 4.04, 5, 3, 4.04, 5);
		
		StatisticsPipe sp = StatisticsPipe.createMode();
		Block b = sp.transform(sb);

		assertThat(b.values().length, is(1));
		assertThat(dblsOf(b.values()), areCloseTo(4.04));
		
	}
	
	@Test
	public void test_range_integers() {
		SimpleBlock sb = new SimpleBlock();
		sb.add(1, 2, 4, 3, 4, 4, 5, 3, 4, 5);
		
		StatisticsPipe sp = StatisticsPipe.createRange();
		Block b = sp.transform(sb);

		assertThat(b.values().length, is(1));
		assertThat(dblsOf(b.values()), areCloseTo(4.0));
		
	}
	
	@Test
	public void test_range_decimals() {
		SimpleBlock sb = new SimpleBlock();
		sb.add(1, 2, 4, 3, 4, 4, 5, 3, 4, 5, 5.01, 0.99);
		
		StatisticsPipe sp = StatisticsPipe.createRange();
		Block b = sp.transform(sb);

		assertThat(b.values().length, is(1));
		assertThat(dblsOf(b.values()), areCloseTo(4.02));
		
	}
	
	
	
}
