package pipez;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;
import static pipez.util.TestUtils.*;

public class DecryptPipeTest {

	@Test
	public void test_key2() {
		SimpleBlock sb = new SimpleBlock("cde");
		
		DecryptPipe select = DecryptPipe.create(2);
		Block b = select.transform(sb);

		assertThat(b.values().length, is(1));
		assertThat(b.values(), are("abc"));
		
	}
	
	@Test
	public void test_key2_numbers() {
		SimpleBlock sb = new SimpleBlock("345", "678");
		
		DecryptPipe select = DecryptPipe.create(2);
		Block b = select.transform(sb);

		assertThat(b.values().length, is(2));
		assertThat(b.values(), are("123", "456"));
		
	}
	
	@Test
	public void test_sentence() {
		SimpleBlock sb = new SimpleBlock("Ymnx", "nx", "f", "yjxy");
		
		DecryptPipe select = DecryptPipe.create(5);
		Block b = select.transform(sb);

		assertThat(b.values().length, is(4));
		assertThat(b.values(), are("This", "is", "a", "test"));
		
	}
	
	
}
