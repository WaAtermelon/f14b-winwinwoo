package pipez;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;
import static pipez.util.TestUtils.*;

public class EncryptPipeTest {

	@Test
	public void test_key2() {
		SimpleBlock sb = new SimpleBlock("abc");
		
		EncryptPipe select = EncryptPipe.create(2);
		Block b = select.transform(sb);

		assertThat(b.values().length, is(1));
		assertThat(b.values(), are("cde"));
		
	}
	
	@Test
	public void test_key2_numbers() {
		SimpleBlock sb = new SimpleBlock("123", "456");
		
		EncryptPipe select = EncryptPipe.create(2);
		Block b = select.transform(sb);

		assertThat(b.values().length, is(2));
		assertThat(b.values(), are("345", "678"));
		
	}
	
	@Test
	public void test_sentence() {
		SimpleBlock sb = new SimpleBlock("This", "is", "a", "test");
		
		EncryptPipe select = EncryptPipe.create(5);
		Block b = select.transform(sb);

		assertThat(b.values().length, is(4));
		assertThat(b.values(), are("Ymnx", "nx", "f", "yjxy"));
		
	}
	
	
}
