package pipez;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;
import static pipez.util.TestUtils.*;

public class EncryptAndDecryptPipeTest {

	@Test
	public void test_key0() {
		//Test that encrypting and decrypting with key 0 will not affect the original block
		SimpleBlock sb = new SimpleBlock("abc");
		
		EncryptPipe select = EncryptPipe.create(0);
		Block b = select.transform(sb);
		
		
		assertThat(b.values().length, is(1));
		assertThat(b.values(), are("abc"));
		
		DecryptPipe select2 = DecryptPipe.create(0);
		Block b2 = select2.transform(sb);

		assertThat(b2.values().length, is(1));
		assertThat(b2.values(), are("abc"));	
		
	}
	
	
	@Test
	public void test_Enc_key2_wrongKey_Dec_key1() {
//Test encrypting with key 2, then decrypting with key 1	
		SimpleBlock sb = new SimpleBlock("Hi there!");
		
		EncryptPipe select = EncryptPipe.create(2);
		Block b = select.transform(sb);
		
		
		assertThat(b.values().length, is(1));
		assertThat(b.values(), are("Jk\"vjgtg#"));
		
		DecryptPipe select2 = DecryptPipe.create(1);
		Block b2 = select2.transform(b);

		assertThat(b2.values().length, is(1));
		assertThat(b2.values(), are("Ij!uifsf\""));
		
		assertNotEquals(b2.values(),b.values());
		
	}

	
	@Test
	public void test_key2_2data_love_message() {
		SimpleBlock love_message = new SimpleBlock("520.Xxx", "1314");
		
		EncryptPipe encrypt = EncryptPipe.create(2);
		Block encryptedMail = encrypt.transform(love_message);

		assertThat(encryptedMail.values().length, is(2));
		assertThat(encryptedMail.values(), are("7420Zzz", "3536"));
		
		// compare original love_message with mail which is encrypted
		// result should not be equal
		assertNotEquals(love_message.values(),encryptedMail.values());

		
		SimpleBlock receive = new SimpleBlock(encryptedMail.values()[0], encryptedMail.values()[1]);
		DecryptPipe decrypt = DecryptPipe.create(2);
		Block decryptedMail = decrypt.transform(receive);

		assertThat(decryptedMail.values().length, is(2));
		assertThat(decryptedMail.values(), are("520.Xxx", "1314"));
		assertThat(love_message.values(), are(decryptedMail.values()));
		
	}
	
	
	@Test
	public void test_sentence() {
		SimpleBlock sentence = new SimpleBlock("Super", "Mario", "is", "Gg", "!");
		
		EncryptPipe encrypt = EncryptPipe.create(5);
		Block encrypted = encrypt.transform(sentence);
		
		assertNotEquals(encrypted.values(),sentence.values());
		assertThat(encrypted.values().length, is(5));
		assertThat(encrypted.values(), are("Xzujw", "Rfwnt", "nx", "Ll", "&"));


		SimpleBlock storedData = new SimpleBlock(encrypted.values()[0],encrypted.values()[1],encrypted.values()[2],encrypted.values()[3],encrypted.values()[4]);
				
		DecryptPipe decrypt = DecryptPipe.create(5);
		Block decrypted = decrypt.transform(storedData);

		assertThat(decrypted.values().length, is(5));
		assertThat(decrypted.values(), are("Super", "Mario", "is", "Gg", "!"));
		
		assertNotEquals(sentence.values(),storedData.values());

		assertNotEquals(encrypted.values(),decrypted.values());
		
	}
		
}
