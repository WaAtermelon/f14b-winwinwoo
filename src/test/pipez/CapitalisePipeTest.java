package pipez;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;
import static pipez.CapitalisePipe.*;
import static pipez.util.TestUtils.*;
import static pipez.core.Utils.*;

public class CapitalisePipeTest {

	@Test
	public void test_letters() throws Exception {
		SimpleBlock block = new SimpleBlock(); 
		block.add("a", "b", "c", "d", "e");
		
		CapitalisePipe cp = CapitalisePipe.create();
		
		Block b = cp.transform(block);
		assertThat(b.fields().length, is(5));
		assertThat(b.values(), are("A", "B", "C", "D", "E"));
	}
	
	@Test
	public void test_words() throws Exception {
		SimpleBlock block = new SimpleBlock(); 
		block.add("This", "is", "a", "test");
		
		CapitalisePipe cp = CapitalisePipe.create();
		
		Block b = cp.transform(block);
		assertThat(b.fields().length, is(4));
		assertThat(b.values(), are("THIS", "IS", "A", "TEST"));
	}
	
	@Test
	public void test_numbers() throws Exception {
		SimpleBlock block = new SimpleBlock(); 
		block.add("1", "2", "3", "4");
		
		CapitalisePipe cp = CapitalisePipe.create();
		
		Block b = cp.transform(block);
		assertThat(b.fields().length, is(4));
		assertThat(b.values(), are("1", "2", "3", "4"));
	}
	
	@Test
	public void test_combined_letters_numbers() throws Exception {
		SimpleBlock block = new SimpleBlock(); 
		block.add("a1", "b2", "c3", "d4");
		
		CapitalisePipe cp = CapitalisePipe.create();
		
		Block b = cp.transform(block);
		assertThat(b.fields().length, is(4));
		assertThat(b.values(), are("A1", "B2", "C3", "D4"));
	}
	
	
}