package pipez;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;

import static pipez.util.TestUtils.*;
import static pipez.core.Utils.*;

public class SortFieldsPipeTest {

	@Test
	public void test_positive_integers() throws Exception {
		SimpleBlock block = new SimpleBlock(); 
		block.add(5, 4, 3, 2, 1);
		
		SortFieldsPipe sfp = SortFieldsPipe.create();
		
		Block b = sfp.transform(block);
		assertThat(b.fields().length, is(5));
		assertThat(dblsOf(b.values()), areCloseTo(1, 2, 3, 4, 5));
	}
	
	@Test
	public void test_negative_integers() throws Exception {
		SimpleBlock block = new SimpleBlock(); 
		block.add(-1, -2, -3, -4, -5);
		
		SortFieldsPipe sfp = SortFieldsPipe.create();
		
		Block b = sfp.transform(block);
		assertThat(b.fields().length, is(5));
		assertThat(dblsOf(b.values()), areCloseTo(-1, -2, -3, -4, -5));
	}
	
	@Test
	public void test_letters() throws Exception {
		SimpleBlock block = new SimpleBlock(); 
		block.add("e", "b", "d", "a", "c");
		
		SortFieldsPipe sfp = SortFieldsPipe.create();
		
		Block b = sfp.transform(block);
		assertThat(b.fields().length, is(5));
		assertThat(b.values(), are("a", "b", "c", "d", "e"));
	}
	
	@Test
	public void test_words() throws Exception {
		SimpleBlock block = new SimpleBlock(); 
		block.add("This", "is", "a", "test");
		
		SortFieldsPipe sfp = SortFieldsPipe.create();
		
		Block b = sfp.transform(block);
		assertThat(b.fields().length, is(4));
		assertThat(b.values(), are("a", "is", "test", "This"));
	}
	
	@Test
	public void test_case() throws Exception {
		SimpleBlock block = new SimpleBlock("XYZ", "abc"); 

		SortFieldsPipe sfp = SortFieldsPipe.create();
		
		Block b = sfp.transform(block);
		assertThat(b.fields().length, is(2));
		assertThat(b.values(), are("abc", "XYZ"));
	}
	
}