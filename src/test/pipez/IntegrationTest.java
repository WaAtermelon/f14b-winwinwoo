package pipez;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;
import static pipez.ArithmeticPipe.*;
import static pipez.util.TestUtils.*;
import static pipez.core.Utils.*;

public class IntegrationTest {

	@Test
	public void test_Arithmeticadd_Identity_Reverse_Odd_Even() throws Exception {
		SimpleBlock block = new SimpleBlock(); 
		block.add(1,2,3,4,5);
		
		ArithmeticPipe ap = ArithmeticPipe.create(
				Add("C1", 1.0), Add("C2", 2.0), Add("C3", 3.0), Add("C4", 4.0), Add("C5", 5.0));
		
		IdentityPipe ip=IdentityPipe.create();
		ReversePipe rp=ReversePipe.create();
		OddFieldsPipe op=OddFieldsPipe.create();
		EvenFieldsPipe ep=EvenFieldsPipe.create();
		Block b = ap.transform(block);
		Block c = ip.transform(b);
		Block d = rp.transform(c);
		Block e = op.transform(d);
		Block f = ep.transform(e);
		assertThat( dblsOf(b.values()), areCloseTo(2.0,4.0,6.0,8.0,10.0));
		assertThat( dblsOf(e.values()), areCloseTo(10.0, 6.0, 2.0));
		assertThat( dblsOf(f.values()), areCloseTo(6.0));
	}
	
	@Test
	public void test_encrypt_captalise_decrypt_nfield_valuematch_fieldmatchinverse() throws Exception {
		SimpleBlock block = new SimpleBlock(); 
		block.add("a1","b2","c3","d4","e5");
		EncryptPipe ep=EncryptPipe.create(1);
		CapitalisePipe cp=CapitalisePipe.create();
		DecryptPipe dp = DecryptPipe.create(1);
		NFieldPipe np = NFieldPipe.create(1, -3);
		ValueMatchPipe vp = ValueMatchPipe.create("C3");
		FieldMatchPipe fp = FieldMatchPipe.createInverse("C2");
		Block b = ep.transform(block);
		Block c = cp.transform(b);
		Block d = dp.transform(c);
		Block e = np.transform(d);
		Block f = vp.transform(e);
		Block g = fp.transform(f);
		assertThat((b.values()), are("b2", "c3", "d4", "e5", "f6"));
		assertThat((d.values()), are("A1", "B2", "C3", "D4", "E5"));
		assertThat(e.fields().length, is(2));
		assertThat((e.values()), are("A1", "C3"));
		assertThat((f.values()), are("A1", "C3"));
		assertThat((g.values()), are("A1", "C3"));

	
}
}