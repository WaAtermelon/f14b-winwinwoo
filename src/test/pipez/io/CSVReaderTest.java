package pipez.io;

import static org.hamcrest.CoreMatchers.is; //eclipse mistakenly marks this as deprecated
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.eq;

import java.io.PrintStream;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SpecialBlocks;

public class CSVReaderTest {

	@Test
	public void test_nonexisting_file() {
		Reader csv = CSVReader.from("test_data\this_does_not_exist");
		
		PrintStream err = mock(PrintStream.class);
		PrintStream orierr = System.err;
		System.setErr(err);
		
		csv.open(); //you should see an error message
		verify(err).println(eq("The file (test_data\this_does_not_exist) could not be found."));
		System.setErr(orierr);
		
	}
	
	@Test
	public void test_empty_file() {
		Reader csv = CSVReader.from("test_data/0_line_0_col.csv");
		assertThat(csv.next(), is(SpecialBlocks.SKIP_BLOCK));
		assertThat(csv.hasNext(), is(false));
		csv.close();
	}
	
	@Test
	public void test_empty_lines() {
		Reader csv = CSVReader.from("test_data/5_line_0_col.csv");
		int numLines = 0;
		while(csv.hasNext()) {
			Block b = csv.next();
			assertThat(b.fields().length, is(0));
			numLines++;
		}
		assertThat(numLines, is(5));
		csv.close();
	}
	
	@Test
	public void test_one_line_one_col() {
		
		Reader csv = CSVReader.from("test_data/1_line_1_col.csv");
		int numLines = 0;
		assertThat(csv.hasNext(), is(true));
		while(csv.hasNext()) {
			Block b = csv.next();
			assertThat(b.fields().length, is(1));
			assertThat(b.value(b.fields()[0]), is("l1c1"));
			numLines++;
		}
		assertThat(numLines, is(1));
		csv.close();
	}

	@Test
	public void test_one_line_four_col() {
		
		Reader csv = CSVReader.from("test_data/1_line_4_col.csv");
		int numLines = 0;
		assertThat(csv.hasNext(), is(true));
		while(csv.hasNext()) {
			Block b = csv.next();
			assertThat(b.fields().length, is(4));
			for(int i=0; i<4; i++) {
				assertThat(b.value(b.fields()[i]), is("l1c"+(i+1)));
			}
			numLines++;
		}
		assertThat(numLines, is(1));
		csv.close();
	}
	
	@Test
	public void test_ten_line_ten_col() {
		
		Reader csv = CSVReader.from("test_data/10_line_10_col.csv");
		int numLines = 0;
		assertThat(csv.hasNext(), is(true));
		while(csv.hasNext()) {
			Block b = csv.next();
			assertThat(b.fields().length, is(10));
			for(int i=0; i<10; i++) {
				assertThat(b.value(b.fields()[i]), is("l"+ (numLines+1) + "c" +(i+1)));
			}
			numLines++;
		}
		assertThat(numLines, is(10));
		csv.close();
	}
	
	@Test
	public void test_incomplete_lines() throws Exception {
		/**
		 * Test scenarios: 
		 * 
		 * ,l1c2,l1c3,l1c4		should have length 4
		 * l2c1,,l2c3,l2c4		should have length 4
		 * l3c1,l3c2,,l3c4		should have length 4
		 * l4c1,l4c2,l4c3,		should have length 3
		 * ,l5c2,l5c3,    		should have length 3
		 * l6c1,,l6c3,    		should have length 3
		 * l7c1,l7c2,,    		should have length 2
		 * ,,l8c3,l8c4   		should have length 4
		 */
		
		Reader csv = CSVReader.from("test_data/incomplete_lines.csv");
		int numLines = 0;
		assertThat(csv.hasNext(), is(true));
		while (csv.hasNext()) {
			Block b = csv.next();
			
			
			for (int i = 0; i < 4; i++) {
				//Check correct number of fields (columns) in the line
				
				if (numLines < 3 || numLines == 7) {
					assertThat(b.fields().length, is(4));
				} else if ((numLines >= 3 && numLines < 6 ) ) {
					assertThat(b.fields().length, is(3));
				} else if (numLines == 6) {
					assertThat(b.fields().length, is(2));
				}
				
				// Check contents of each field
				// In the first three lines, the missing field is of the form l1c1, l2c2 etc.
				if (((numLines + 1) == (i + 1) && numLines < 3) ) {
					assertThat(b.value(b.fields()[i]), is(""));
				
				// In the fourth line, the last field is missing, and should throw an exception
				// when we try to check that it is ""
				} else if (((numLines + 1) == (i + 1)  && numLines == 3 )) {
					try {	
						assertThat(b.value(b.fields()[i]), is(""));	
					} catch (ArrayIndexOutOfBoundsException e) {
					}
					
				// Similarly, lines 5,6,7 are missing their last field:
				} else if ( (numLines + 1) == 5 && (i + 1) == 4 || (numLines + 1) == 6 && (i + 1) == 4 || (numLines + 1) == 7 && (i + 1) == 4 || ((numLines + 1) == 7 && (i + 1) == 3 )) {
					try {	
						assertThat(b.value(b.fields()[i]), is(""));
					} catch (ArrayIndexOutOfBoundsException e) {
					}
					
				// Also in lines 5-7, there is a missing field of the form l5c1, l6c2 etc.	
				} else if (((numLines + 1) == 5 && (i + 1) == 1 )|| ((numLines + 1) == 6 && (i + 1) == 2 )  || ((numLines + 1) == 8 && (i + 1) == 1) || ((numLines + 1) == 8 && (i + 1) == 2 )) {
					assertThat(b.value(b.fields()[i]), is(""));	
				
				// Line 7 is weird, there are 2 missing fields next to each other
				// so CSVreader only reads the first 2 fields
				// Checking that l7c3 is "" should also throw an array index out of bounds: 
				} else if (((numLines + 1) == 7 && (i + 1) == 3 && numLines == 6 )) {
					try {	
						assertThat(b.value(b.fields()[i]), is(""));	
					} catch (ArrayIndexOutOfBoundsException e) {
					}
				
				// Check that the existing fields in lines 4-8 have been read correctly
				} else if (((numLines + 1) == 4 && (i + 1) == 1 )) {
					assertThat(b.value(b.fields()[i]), is("l4c1"));
				} else if (((numLines + 1) == 4 && (i + 1) == 2 )) {
					assertThat(b.value(b.fields()[i]), is("l4c2"));
				} else if (((numLines + 1) == 4 && (i + 1) == 3 )) {
					assertThat(b.value(b.fields()[i]), is("l4c3"));	
				} else if (((numLines + 1) == 5 && (i + 1) == 2 )) {
					assertThat(b.value(b.fields()[i]), is("l5c2"));	
				} else if (((numLines + 1) == 5 && (i + 1) == 3 )) {
					assertThat(b.value(b.fields()[i]), is("l5c3"));
				} else if (((numLines + 1) == 6 && (i + 1) == 1 )) {
					assertThat(b.value(b.fields()[i]), is("l6c1"));
				} else if (((numLines + 1) == 6 && (i + 1) == 3 )) {
					assertThat(b.value(b.fields()[i]), is("l6c3"));
				} else if (((numLines + 1) == 7 && (i + 1) == 1 )) {
					assertThat(b.value(b.fields()[i]), is("l7c1"));
				} else if (((numLines + 1) == 7 && (i + 1) == 2 )) {
					assertThat(b.value(b.fields()[i]), is("l7c2"));	
				} else if (((numLines + 1) == 8 && (i + 1) == 3 )) {
					assertThat(b.value(b.fields()[i]), is("l8c3"));
				} else if (((numLines + 1) == 8 && (i + 1) == 4 )) {
					assertThat(b.value(b.fields()[i]), is("l8c4"));	
				
				//Check that the existing fields in lines 1-3 have been read correctly
				} else {
					if (numLines < 3)
					assertThat(b.value(b.fields()[i]), is("l"+ (numLines + 1) + "c" +(i + 1)));	
				}
			}	
				numLines++;
		}
		assertThat(numLines, is(8));
		csv.close();
	}
}
