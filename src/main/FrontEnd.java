import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;


import javax.swing.JFrame;
import javax.swing.JOptionPane;
import pipez.*;
import pipez.core.*;
import pipez.io.*;
import java.util.Scanner;

public class FrontEnd {

	public static void main(String[] args) {
		String pipelineName = "";
		String[] choices;
		ArrayList<String> pipeList = new ArrayList<String>();
		String pipeName = "";
		String inputFile = "";
		String outputFile = "";
		boolean wantToAddPipe = true;
		JFrame frame = new JFrame();

		String[] options = { "Create a new pipeline", "Apply an existing pipeline to a CSV file"};
		String select = (String) JOptionPane.showInputDialog(null, "Welcome to the Pipez Wizard!\nWhat would you like to do?", "Welcome to Pipez!", JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

		//If cancelled:
		if (select == null) {
			JOptionPane.showMessageDialog(null, "Cancelled. Pipez exiting", "Cancelled", JOptionPane.INFORMATION_MESSAGE);
			System.exit(0);
		} else if (select.equals("Create a new pipeline")) {
			pipelineName = JOptionPane.showInputDialog(null, "Enter name for pipeline:", "Name pipeline", JOptionPane.PLAIN_MESSAGE);
			while (pipelineName.equals("")) {
				pipelineName = JOptionPane.showInputDialog(null, "Please enter a valid pipeline name.", "Please try again", JOptionPane.WARNING_MESSAGE);
				if (pipelineName == null) {
					JOptionPane.showMessageDialog(null, "Operation cancelled. Pipez exiting", "Operation cancelled", JOptionPane.INFORMATION_MESSAGE);
					System.exit(0);
				}
			}

			//Create an array of choices to be displayed in a drop down menu
			choices = new String[] { "IdentityPipe", "ReversePipe", "EvenFieldsPipe", "OddFieldsPipe", "EncryptPipe", "DecryptPipe", "CapitalisePipe", "SortFieldsPipe", "MaxValuePipe", "MinValuePipe", "FieldMatchPipe", "ValueMatchPipe", "StatisticsPipe"};

			while (wantToAddPipe) {
				//Display drop down
				pipeName = (String) JOptionPane.showInputDialog(null, "Select a pipe to add to the pipeline " + pipelineName + ":", "Add pipe", JOptionPane.QUESTION_MESSAGE, null, choices, choices[0]);
				if (pipeName == null) {
					JOptionPane.showMessageDialog(null, "Cancelled. Pipez exiting", "Cancelled", JOptionPane.INFORMATION_MESSAGE);
					System.exit(0);
				} else {
					pipeList.add(pipeName);	
				}

				int response = 0;
				if (pipeList.size() == 1) {
					response = JOptionPane.showConfirmDialog(frame, "Your pipeline currently contains " + pipeList.size() + " pipe: " + pipeList.toString().replace("[", "").replace("]", "") + "\nWould you like to add another pipe to the pipeline?");
				} else if (pipeList.size() > 1) {
					response = JOptionPane.showConfirmDialog(frame, "Your pipeline currently contains " + pipeList.size() + " pipes:\n" + pipeList.toString().replace("[", "").replace("]", "") + "\nWould you like to add another pipe to the pipeline?");
				}

				if (response == 1) {
					wantToAddPipe = false;
					break;
				} 
			}

			String stringArray[] = { "Save pipeline", "Apply pipeline" };
			String answer = (String) JOptionPane.showInputDialog(null, "Would you like to save your pipeline or apply it to a CSV file without saving?", "Save or Apply Pipeline", JOptionPane.QUESTION_MESSAGE, null, stringArray, stringArray[0]);

			//if cancelled
			if (answer == null) {
				JOptionPane.showMessageDialog(null, "Cancelled. Pipez exiting", "Cancelled", JOptionPane.INFORMATION_MESSAGE);
				System.exit(0);
			//if user wants to save their pipeline
			} else if (answer.equals("Save pipeline")) {
				outputFile = JOptionPane.showInputDialog(null, "Enter path to save your pipeline", "Export pipeline", JOptionPane.PLAIN_MESSAGE);
				if (outputFile == null) {
					JOptionPane.showMessageDialog(null, "Cancelled. Pipez exiting", "Cancelled", JOptionPane.INFORMATION_MESSAGE);
					System.exit(0);
				} else {
					File file = new File(outputFile);
					PrintWriter writer;
					try {
						writer = new PrintWriter(file); 

					} catch (FileNotFoundException e) {
						JOptionPane.showMessageDialog(null, "File not found, System exiting.\nEnsure the .csv file exists and can be accessed at the specified path, and try again.", "Error! File not found!", JOptionPane.ERROR_MESSAGE);
						return;
					}
					writer.print(pipeList.toString().replace("[", "").replace("]", "").replace(",",""));
					writer.close();
					JOptionPane.showMessageDialog(null, "Your pipeline saved successfully to " + outputFile, "Pipeline saved successfully", JOptionPane.INFORMATION_MESSAGE);
					int response = JOptionPane.showConfirmDialog(frame, "Would you like to apply your pipeline to a CSV file?");
					if (response != JOptionPane.YES_OPTION) {
						System.exit(0);
					} else {
						answer = "Apply pipeline";
					}

				}
			}

			//If user wants to apply the pipeline:
			//Ask for input file path
			if (answer.equals("Apply pipeline")) {
				inputFile = JOptionPane.showInputDialog(null, "Enter path for input .csv file:", "Select input file", JOptionPane.PLAIN_MESSAGE);
				if (inputFile == null) {
					JOptionPane.showMessageDialog(null, "Cancelled. Pipez exiting", "Cancelled", JOptionPane.INFORMATION_MESSAGE);
					System.exit(0);
				}
				while (inputFile.contains(".csv") != true) {
					inputFile = JOptionPane.showInputDialog(null, "Please enter a valid input file path, e.g. in/inputfile.csv", "Please try again", JOptionPane.WARNING_MESSAGE);
					if (inputFile == null) {
						JOptionPane.showMessageDialog(null, "Cancelled. Pipez exiting", "Cancelled", JOptionPane.INFORMATION_MESSAGE);
						System.exit(0);
					}
				}
				//This scanner is used to test whether the input file actually exists
				Scanner test;
				File inputFileObject = new File(inputFile);
				try {
					test = new Scanner(inputFileObject);
					test.close();
				} catch (FileNotFoundException e) {
					JOptionPane.showMessageDialog(null, "File not found, System exiting.\nEnsure the .csv file exists and can be accessed at the specified path, and try again.", "Error! File not found!", JOptionPane.ERROR_MESSAGE);
					return;
				}

				//Ask for output file
				outputFile = JOptionPane.showInputDialog(null, "Enter path for output .csv file", "Choose output file path", JOptionPane.PLAIN_MESSAGE);
				if (outputFile == null) {
					JOptionPane.showMessageDialog(null, "Cancelled. Pipez exiting", "Cancelled", JOptionPane.INFORMATION_MESSAGE);
					System.exit(0);
				}
				while (outputFile.contains(".csv") != true) {
					outputFile = JOptionPane.showInputDialog(null, "Please enter a valid output path, e.g. out/outputfile.csv", "Please try again", JOptionPane.WARNING_MESSAGE);
					if (outputFile == null) {
						JOptionPane.showMessageDialog(null, "Cancelled. Pipez exiting", "Cancelled", JOptionPane.INFORMATION_MESSAGE);
						System.exit(0);
					}
				}

				//Apply the pipeline's pipes in order
				String[] pipeNames = pipeList.toArray(new String[pipeList.size()]);
				Pipeline pipeline= new Pipeline(pipelineName);
				pipeline.in(CSVReader.from(inputFile));

				for (int i = 0; i < pipeNames.length; i++) {
					if (pipeNames[i].equalsIgnoreCase("identitypipe")) {
						pipeline.append(IdentityPipe.create());
					} else if (pipeNames[i].equalsIgnoreCase("reversepipe")) {
						pipeline.append(ReversePipe.create());
					} else if (pipeNames[i].equalsIgnoreCase("evenfieldspipe")) {
						pipeline.append(EvenFieldsPipe.create());
					} else if (pipeNames[i].equalsIgnoreCase("oddfieldspipe")) {
						pipeline.append(OddFieldsPipe.create());
					} else if (pipeNames[i].equalsIgnoreCase("sortfieldspipe")) {
						pipeline.append(SortFieldsPipe.create());
					} else if (pipeNames[i].equalsIgnoreCase("capitalisepipe")) {
						pipeline.append(CapitalisePipe.create());
					} else if (pipeNames[i].equalsIgnoreCase("maxvaluepipe")) {
						pipeline.append(MaxValuePipe.create());
					} else if (pipeNames[i].equalsIgnoreCase("minvaluepipe")) {
						pipeline.append(MinValuePipe.create());
					} else if (pipeNames[i].equalsIgnoreCase("encryptpipe")) {
						String key = JOptionPane.showInputDialog(null, "Enter encryption key", "Enter encryption key", JOptionPane.PLAIN_MESSAGE);
						while (key == null) {
							key = JOptionPane.showInputDialog(null, "Please enter a valid encryption key. Encryption key must be an integer greater than zero", "Please try again", JOptionPane.WARNING_MESSAGE);
						}
						int keyNum = 0;
						while (keyNum == 0) {
							try {
								keyNum = Integer.parseInt(key);
							} catch (NumberFormatException e) {
								key = JOptionPane.showInputDialog(null, "Please enter a valid encryption key. Encryption key must be an integer greater than zero", "Please try again", JOptionPane.WARNING_MESSAGE);
								
							}
						}
						pipeline.append(EncryptPipe.create(keyNum));
						
					} else if (pipeNames[i].equalsIgnoreCase("decryptpipe")) {
						String key = JOptionPane.showInputDialog(null, "Enter decryption key", "Enter decryption key", JOptionPane.PLAIN_MESSAGE);
						while (key == null) {
							key = JOptionPane.showInputDialog(null, "Please enter a valid decryption key. decryption key must be an integer greater than zero", "Please try again", JOptionPane.WARNING_MESSAGE);
						}
						int keyNum = 0;
						while (keyNum == 0) {
							try {
								keyNum = Integer.parseInt(key);
							} catch (NumberFormatException e) {
								key = JOptionPane.showInputDialog(null, "Please enter a valid decryption key. decryption key must be an integer greater than zero", "Please try again", JOptionPane.WARNING_MESSAGE);	
							}
						}
						pipeline.append(DecryptPipe.create(keyNum));
					} else if (pipeNames[i].equalsIgnoreCase("fieldmatchpipe")) {
						String toMatch = JOptionPane.showInputDialog(null, "Enter search string for FieldMatch", "Input search string", JOptionPane.PLAIN_MESSAGE);
						while (toMatch == null) {
							toMatch = JOptionPane.showInputDialog(null, "Please enter a valid search string.", "Please try again", JOptionPane.WARNING_MESSAGE);
						}
						pipeline.append(FieldMatchPipe.create(toMatch));
					} else if (pipeNames[i].equalsIgnoreCase("valuematchpipe")) {
						String toMatch = JOptionPane.showInputDialog(null, "Enter search string for ValueMatch", "Input search string", JOptionPane.PLAIN_MESSAGE);
						while (toMatch == null) {
							toMatch = JOptionPane.showInputDialog(null, "Please enter a valid search string.", "Please try again", JOptionPane.WARNING_MESSAGE);
						}
						
						pipeline.append(ValueMatchPipe.create(toMatch));
					} else if (pipeNames[i].equalsIgnoreCase("statisticspipe")) {
						String[] statistics = {"Mean", "Median", "Mode", "Range"};
						String choice = (String) JOptionPane.showInputDialog(null, "Select operation", "Choose Operation", JOptionPane.QUESTION_MESSAGE, null, statistics, statistics[0]);
							if (choice.equals("Mean")) {
								pipeline.append(StatisticsPipe.createMean());
							} else if (choice.equals("Median")) {
								pipeline.append(StatisticsPipe.createMedian());
							} else if (choice.equals("Mode")) {
								pipeline.append(StatisticsPipe.createMode());
							} else if (choice.equals("Range")) {
								pipeline.append(StatisticsPipe.createRange());
							} 
	
					} 
				}

				//Apply the pipeline and output it to the specified output file
				pipeline.out(CSVWriter.to(outputFile));

				//					} else if (pipeName.equalsIgnoreCase("fieldmatchpipe")) {
				//						String toMatch = JOptionPane.showInputDialog(null, "Enter search", "Input search string");
				//						Pipeline.create(pipelineName)
				//						.in(CSVReader.from(inputFile))
				//						.append(OddFieldsPipe.create())
				//						.out(CSVWriter.to(outputFile));

				//Display message
				JOptionPane.showMessageDialog(null, "Successfully applied pipeline " + pipelineName + " to CSV file " + inputFile + "!\n Results saved successfully to " + outputFile, "Pipez Result", JOptionPane.INFORMATION_MESSAGE);
				System.exit(0);
			}
		}


		else if (select.equals ("Apply an existing pipeline to a CSV file")) {
			//TODO

			//Import existing pipeline
			String existingPipeline = "";
			existingPipeline = JOptionPane.showInputDialog(null, "Enter path for existing pipeline:", "Import existing pipeline", JOptionPane.PLAIN_MESSAGE);
			if (existingPipeline== null) {
				JOptionPane.showMessageDialog(null, "Cancelled. Pipez exiting", "Cancelled", JOptionPane.INFORMATION_MESSAGE);
				System.exit(0);
			}


			Scanner test;
			File importFileObject = new File(existingPipeline);
			try {
				test = new Scanner(importFileObject);
				test.close();
			} catch (FileNotFoundException e) {
				JOptionPane.showMessageDialog(null, "File not found, System exiting.\nEnsure the .csv file exists and can be accessed at the specified path, and try again.", "Error! File not found!", JOptionPane.ERROR_MESSAGE);
				return;
			}

			//Ask for input file path
			inputFile = JOptionPane.showInputDialog(null, "Enter path for input .csv file:", "Select input file", JOptionPane.PLAIN_MESSAGE);
			if (inputFile == null) {
				JOptionPane.showMessageDialog(null, "Cancelled. Pipez exiting", "Cancelled", JOptionPane.INFORMATION_MESSAGE);
				System.exit(0);
			}
			while (inputFile.contains(".csv") != true) {
				inputFile = JOptionPane.showInputDialog(null, "Please enter a valid input file path, e.g. in/inputfile.csv", "Please try again", JOptionPane.WARNING_MESSAGE);
				if (inputFile == null) {
					JOptionPane.showMessageDialog(null, "Cancelled. Pipez exiting", "Cancelled", JOptionPane.INFORMATION_MESSAGE);
					System.exit(0);
				}
			}
			//This scanner is used to test whether the input file actually exists
			Scanner test2;
			File inputFileObject = new File(inputFile);
			try {
				test2 = new Scanner(inputFileObject);
				test2.close();
			} catch (FileNotFoundException e) {
				JOptionPane.showMessageDialog(null, "File not found, System exiting.\nEnsure the .csv file exists and can be accessed at the specified path, and try again.", "Error! File not found!", JOptionPane.ERROR_MESSAGE);
				return;
			}

			//Ask for output file
			outputFile = JOptionPane.showInputDialog(null, "Enter path for output .csv file", "Choose output file path", JOptionPane.PLAIN_MESSAGE);
			if (outputFile == null) {
				JOptionPane.showMessageDialog(null, "Cancelled. Pipez exiting", "Cancelled", JOptionPane.INFORMATION_MESSAGE);
				System.exit(0);
			}
			while (outputFile.contains(".csv") != true) {
				outputFile = JOptionPane.showInputDialog(null, "Please enter a valid output path, e.g. out/outputfile.csv", "Please try again", JOptionPane.WARNING_MESSAGE);
				if (outputFile == null) {
					JOptionPane.showMessageDialog(null, "Cancelled. Pipez exiting", "Cancelled", JOptionPane.INFORMATION_MESSAGE);
					System.exit(0);
				}
			}

			//Read in the pipes from the file
			//Apply the pipeline's pipes in order
			Scanner scan;
			try {
				scan = new Scanner(importFileObject);
				scan.close();
			} catch (FileNotFoundException e) {
				JOptionPane.showMessageDialog(null, "File not found, System exiting.\nEnsure the .csv file exists and can be accessed at the specified path, and try again.", "Error! File not found!", JOptionPane.ERROR_MESSAGE);
				return;
			}
			String[] pipeNames = scan.nextLine().split(" ");
			Pipeline pipeline= new Pipeline(pipelineName);
			pipeline.in(CSVReader.from(inputFile));

			for (int i = 0; i < pipeNames.length; i++) {
				if (pipeNames[i].equalsIgnoreCase("identitypipe")) {
					pipeline.append(IdentityPipe.create());
				} else if (pipeNames[i].equalsIgnoreCase("reversepipe")) {
					pipeline.append(ReversePipe.create());
				} else if (pipeNames[i].equalsIgnoreCase("evenfieldspipe")) {
					pipeline.append(EvenFieldsPipe.create());
				} else if (pipeNames[i].equalsIgnoreCase("oddfieldspipe")) {
					pipeline.append(OddFieldsPipe.create());
				} else if (pipeNames[i].equalsIgnoreCase("sortfieldspipe")) {
					pipeline.append(SortFieldsPipe.create());
				} else if (pipeNames[i].equalsIgnoreCase("capitalisepipe")) {
					pipeline.append(CapitalisePipe.create());
				} else if (pipeNames[i].equalsIgnoreCase("maxvaluepipe")) {
					pipeline.append(MaxValuePipe.create());
				} else if (pipeNames[i].equalsIgnoreCase("minvaluepipe")) {
					pipeline.append(MinValuePipe.create());
				} else if (pipeNames[i].equalsIgnoreCase("encryptpipe")) {
					String key = JOptionPane.showInputDialog(null, "Enter encryption key", "Enter encryption key", JOptionPane.PLAIN_MESSAGE);
					while (key == null) {
						key = JOptionPane.showInputDialog(null, "Please enter a valid encryption key. Encryption key must be an integer greater than zero", "Please try again", JOptionPane.WARNING_MESSAGE);
					}
					int keyNum = 0;
					while (keyNum == 0) {
						try {
							keyNum = Integer.parseInt(key);
						} catch (NumberFormatException e) {
							key = JOptionPane.showInputDialog(null, "Please enter a valid encryption key. Encryption key must be an integer greater than zero", "Please try again", JOptionPane.WARNING_MESSAGE);
							
						}
					}
					pipeline.append(EncryptPipe.create(keyNum));
					
				} else if (pipeNames[i].equalsIgnoreCase("decryptpipe")) {
					String key = JOptionPane.showInputDialog(null, "Enter decryption key", "Enter decryption key", JOptionPane.PLAIN_MESSAGE);
					while (key == null) {
						key = JOptionPane.showInputDialog(null, "Please enter a valid decryption key. decryption key must be an integer greater than zero", "Please try again", JOptionPane.WARNING_MESSAGE);
					}
					int keyNum = 0;
					while (keyNum == 0) {
						try {
							keyNum = Integer.parseInt(key);
						} catch (NumberFormatException e) {
							key = JOptionPane.showInputDialog(null, "Please enter a valid decryption key. decryption key must be an integer greater than zero", "Please try again", JOptionPane.WARNING_MESSAGE);	
						}
					}
					pipeline.append(DecryptPipe.create(keyNum));
				} else if (pipeNames[i].equalsIgnoreCase("fieldmatchpipe")) {
					String toMatch = JOptionPane.showInputDialog(null, "Enter search string for FieldMatch", "Input search string", JOptionPane.PLAIN_MESSAGE);
					while (toMatch == null) {
						toMatch = JOptionPane.showInputDialog(null, "Please enter a valid search string.", "Please try again", JOptionPane.WARNING_MESSAGE);
					}
					
					pipeline.append(FieldMatchPipe.create(toMatch));
				} else if (pipeNames[i].equalsIgnoreCase("valuematchpipe")) {
					String toMatch = JOptionPane.showInputDialog(null, "Enter search string for ValueMatch", "Input search string", JOptionPane.PLAIN_MESSAGE);
					while (toMatch == null) {
						toMatch = JOptionPane.showInputDialog(null, "Please enter a valid search string.", "Please try again", JOptionPane.WARNING_MESSAGE);
					}
					
					pipeline.append(ValueMatchPipe.create(toMatch));
				} else if (pipeNames[i].equalsIgnoreCase("statisticspipe")) {
					String[] statistics = {"Mean", "Median", "Mode", "Range"};
					String choice = (String) JOptionPane.showInputDialog(null, "Select operation", "Choose Operation", JOptionPane.QUESTION_MESSAGE, null, statistics, statistics[0]);
						if (choice.equals("Mean")) {
							pipeline.append(StatisticsPipe.createMean());
						} else if (choice.equals("Median")) {
							pipeline.append(StatisticsPipe.createMedian());
						} else if (choice.equals("Mode")) {
							pipeline.append(StatisticsPipe.createMode());
						} else if (choice.equals("Range")) {
							pipeline.append(StatisticsPipe.createRange());
						} 
				} 
			}

			//Apply the pipeline and output it to the specified output file
			pipeline.out(CSVWriter.to(outputFile));
			JOptionPane.showMessageDialog(null, "Successfully applied pipeline " + pipelineName + " to CSV file " + inputFile + "!\n Results saved successfully to " + outputFile, "Pipez Result", JOptionPane.INFORMATION_MESSAGE);
			System.exit(0);
		}


		//JOptionPane.showMessageDialog(null, "Welcome to the Pipez wizard, which will allow you to apply a single pipe to a CSV file.\nPress OK to continue.", "Welcome to Pipez!", JOptionPane.INFORMATION_MESSAGE);


		//String pipeName = JOptionPane.showInputDialog(null, "Enter the pipe you would like to apply.\nCurrently supported options are: IdentityPipe, ReversePipe, EvenFieldsPipe, OddFieldsPipe", "Select pipe", JOptionPane.PLAIN_MESSAGE);


	}
}


