package pipez;

import pipez.core.Block;
import pipez.core.Pipe;
import pipez.core.SimpleBlock;


/**
 * Encrypt the data of fields in blocks with key which provided by user.
 * e.g. abc is current data, encrypt with key provided by user e.g. 2
 * 		replace a -> c
 * 		replace b -> d
 *		replace c -> e
 * 		data encrypted becomes cde
 * @author Team WinWinWoo
 *
 */
public class EncryptPipe implements Pipe{

	//The key that will be used for encryption
	private int key;

	//Constructor: Takes in one integer argument which is the key to be used for encryption
	private EncryptPipe(int key) {
		this.key = key;
	}

	//Creates a new EncryptPipe with the key specified 
	public static EncryptPipe create(int key) {
		return new EncryptPipe(key);
	}

	@Override
	public String getName() {
		return "Encrypt data";
	}

	
	@Override
	public Block transform(Block block) {
		String newString = ""; //Create a new String which will be used to store the encrypted characters of each field
		//Create a new block which will be used to store the encrypted data
		SimpleBlock encryptBlock = new SimpleBlock();
		for (int i = 0; i < block.values().length; i++) { //For each field:
			newString = ""; //reset string for each field
			for (int j = 0; j < block.values()[i].length(); j++) { //For each character in the field
				char charAtIndex = block.values()[i].charAt(j);
//				System.out.println(charAtIndex +" is the char before encrypting at index " + j);
				charAtIndex += key; //encrypt the character by adding the key to it
//				System.out.println(charAtIndex + " is the char after encrypting at index " + j );
				newString += charAtIndex; //Add the encrypted character to the String for that field
//				System.out.println(newString);
			}
//			System.out.println(newString);
			encryptBlock.add(block.fields()[i], newString); //Add the encrypted field to the new block
//			System.out.println(encryptBlock.values()[i]);
		}
		return encryptBlock;
	}


}


