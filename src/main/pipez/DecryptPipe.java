package pipez;

import pipez.core.Block;
import pipez.core.Pipe;
import pipez.core.SimpleBlock;


/**
 * Decrypt the data of fields in blocks with key which provided by user.
 * e.g. edc is current data, decrypt with key provided by user e.g. 2
 * 		replace e -> c
 * 		replace d -> b
 *		replace c -> a
 * 		data decrypted becomes cba
 * @author Team WinWinWoo
 *
 */
public class DecryptPipe implements Pipe{

	//The key that will be used for decryption
	private int key;

	//Constructor: Takes in one integer argument which is the key to be used for decryption
	private DecryptPipe(int key) {
		this.key = key;
	}

	//Creates a new DecryptPipe with the key specified
	public static DecryptPipe create(int key) {
		return new DecryptPipe(key);
	}

	@Override
	public String getName() {
		return "Decrypt data";
	}


	@Override
	public Block transform(Block block) {
		String newString = ""; //Create a new String which will be used to store the decrypted characters of each field
		//Create a new block which will be used to store the decrypted data
		SimpleBlock decryptBlock = new SimpleBlock();
		for (int i = 0; i < block.values().length; i++) { //For each field:
			newString = ""; //reset string for each field
			for (int j = 0; j < block.values()[i].length(); j++) { //For each character in the field
				char charAtIndex = block.values()[i].charAt(j);
//				System.out.println(charAtIndex +" is the char before decrypting at index " + j);
				charAtIndex -= key; //decrypt the character by adding the key to it
//				System.out.println(charAtIndex + " is the char after decrypting at index " + j );
				newString += charAtIndex; //Add the decrypted character to the String for that field
//				System.out.println(newString);
			}
//			System.out.println(newString);
			decryptBlock.add(block.fields()[i], newString); //Add the decrypted field to the new block
//			System.out.println(decryptBlock.values()[i]);
		}
		return decryptBlock;
	}


}
