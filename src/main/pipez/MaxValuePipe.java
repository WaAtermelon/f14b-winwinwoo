package pipez;

import pipez.core.Block;
import pipez.core.Pipe;
import pipez.core.SimpleBlock;

import static pipez.core.SpecialBlocks.*;

/**
 * Returns the maximum value out of the fields.
 * 
 * @author slin3916
 *
 */
public class MaxValuePipe implements Pipe {

	@Override
	public String getName() {
		return "Maximum Value";
	}
	
	private MaxValuePipe() {
	}

	public static MaxValuePipe create() {
		return new MaxValuePipe();
	}

	@Override
	public Block transform(Block block) {
		SimpleBlock nb = new SimpleBlock();
		double max;
		try {
			max = Double.parseDouble(block.values()[0]);  //set the 1st number as the maximum
		} catch (NumberFormatException e) { //NumberFormatException will be thrown if there is non-numerical data
			return nb;
		}
		int maxIndex = 0;
		for (int i = 0; i < block.values().length; i++) {
			try {
				if (Double.parseDouble(block.values()[i]) > max) {
					max = Double.parseDouble(block.values()[i]); //update the maximum
					maxIndex = i;
				}
			} catch (NumberFormatException e) {
				return nb;
			}
		} 
		nb.add(block.fields()[maxIndex], Double.toString(max));
		return nb;
	}

}
