package pipez;

import pipez.core.Block;
import pipez.core.Pipe;
import pipez.core.SimpleBlock;

import static pipez.core.SpecialBlocks.*;

import java.lang.Double;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Performs statistical operations.
 * 
 * @author slin3916
 *
 */
public class StatisticsPipe implements Pipe {

	public static double calculateFrequency(double argument, Block block) {
		int count = 0;
		for (int i = 0; i < block.values().length; i++) {
			if (Double.compare(Double.parseDouble(block.values()[i]) , argument) == 0) {
				count++;
			}
		}
		return count;
	}
	@Override
	public String getName() {
		return "Statistics";
	}

	private double argument = 0; //needed to find the frequency of a particular item
	private String operation = null;
	
	private StatisticsPipe(String operation) {
		this.operation = operation;
	}
	
	private StatisticsPipe(String operation, double argument) {
		this.operation = operation;
		this.argument = argument;
	}
	
	
	public static StatisticsPipe createFrequency(double argument) {
		return new StatisticsPipe("Frequency", argument);
	}
	
	public static StatisticsPipe createMean() {
		return new StatisticsPipe("Mean");
	}

	public static StatisticsPipe createMode() {
		return new StatisticsPipe("Mode");
	}
	
	public static StatisticsPipe createMedian() {
		return new StatisticsPipe("Median");
	}
	
	public static StatisticsPipe createRange() {
		return new StatisticsPipe("Range");
	}
	
	@Override
	public Block transform(Block block) {
		SimpleBlock sb = new SimpleBlock();
		//Calculate mean
		if (operation.equals("Mean")) {
			double sum = 0;
			int length = 0;
			double mean = 0;
			for (String s : block.values()) {
				try {
					sum += Double.parseDouble(s);
					length++;
				} catch (NumberFormatException e) {
					continue;
				}
				mean = sum / length;
			}
			sb.add(mean);
			return sb;
		}
		
		//Calculate frequency
		if (operation.equals("Frequency")) {
			sb.add(calculateFrequency(argument, block));
			return sb;
		}
		
		//Calculate median
		if (operation.equals("Median")) {
			double[] array = new double [block.values().length];
			for (int i = 0; i < block.values().length; i++) {
				try {
				array[i] = Double.parseDouble(block.values()[i]);
				} catch (NumberFormatException e) {
					continue;
				}
			}
			Arrays.sort(array);
			int length = array.length;
			if (length == 1) {
				sb.add(array[0]);
				return sb;
			} else if (length % 2 != 0) {
				sb.add(array[(length / 2 + 1) - 1]);
				return sb;
			} else {
				double median = ((array[length / 2] + array[(length / 2) - 1]) / 2.0);
				sb.add(median);
				return sb;
				}
		}
		
		if (operation.equals("Mode")) {
			HashMap<Double, Integer> map = new HashMap<Double,Integer>(); //Create new hash map mapping each value to its frequency
			
			for (int i = 0; i < block.values().length; i++) {
				if (map.containsKey(Double.parseDouble(block.values()[i]))) {
					// Increment the count
					map.put(Double.parseDouble(block.values()[i]), map.get(Double.parseDouble(block.values()[i])) + 1);
				} else {
					// Insert c into the map with count 1
					map.put(Double.parseDouble(block.values()[i]), 1);
				}
			}
				
			
			int maxFrequency = 0;
			double mode = 0;
			for (HashMap.Entry<Double, Integer> entry : map.entrySet()) {
				Double key = entry.getKey();
			    Integer value = entry.getValue();
			    if (value > maxFrequency) {
			    	maxFrequency = value;
			    	mode = key;
			    }
			}
				sb.add(mode);
				return sb;	
			}
		
		if (operation.equals("Range")) {
			double[] array = new double [block.values().length];
			for (int i = 0; i < block.values().length; i++) {
				try {
				array[i] = Double.parseDouble(block.values()[i]);
				} catch (NumberFormatException e) {
					continue;
				}
			}
			Arrays.sort(array); //sort array so smallest number is first, largest number last
			double range = array[array.length - 1] - array[0]; //The range is always largest number - smallest number
			sb.add(range);
			return sb;
		}
			
		
		
//		if (inverse == false && ignoreCase == true) {
//			for (String v : block.fields()) {
//				if (v.equalsIgnoreCase(toMatch)) {
//					return block;
//				}
//		} 
//		} else if (inverse == false && ignoreCase == false) {
//		for (String v : block.fields()) {
//			if (v.equals(toMatch)) {
//				return block;
//			}
//		}
//		} else {
//			//Create an empty block which will be returned when there is a match
//			IdentityPipe id = IdentityPipe.create();
//			SimpleBlock sb = new SimpleBlock();
//			Block b = id.transform(sb);
//			
//			//Test if any of the fields matches with the input string. If so, return the empty block.
//			for (String v : block.fields()) {
//				if (ignoreCase == false && v.equals(toMatch)) {
//					return b;
//				} else if (ignoreCase == true && v.equalsIgnoreCase(toMatch)) {
//					return b;
//				} 
//			}
//		
//			//Return the original block if the input string does not match with any fields. No need to check for ignoreCase because all instances where the input string matches the field name but not its case will have already been accounted for in the previous section.
//			for (String v : block.fields()) {
//				if (v.equals(toMatch) != true) {
//					return block;
//				}
//		}
		
		
//	}
		
		
		return SKIP_BLOCK;
	}

}
