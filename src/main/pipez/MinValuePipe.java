package pipez;

import pipez.core.Block;
import pipez.core.Pipe;
import pipez.core.SimpleBlock;

import static pipez.core.SpecialBlocks.*;

/**
 * Returns the minimum value out of the fields.
 * 
 * @author slin3916
 *
 */
public class MinValuePipe implements Pipe {

	@Override
	public String getName() {
		return "Minimum Value";
	}
	
	private MinValuePipe() {
	}

	public static MinValuePipe create() {
		return new MinValuePipe();
	}

	@Override
	public Block transform(Block block) {
		SimpleBlock nb = new SimpleBlock();
		double min;
		try {
			min = Double.parseDouble(block.values()[0]);
		} catch (NumberFormatException e) { //NumberFormatException will be thrown if there is non-numerical data
			return nb;
		}
		int minIndex = 0;
		for (int i = 0; i < block.values().length; i++) {
			try {
			if (Double.parseDouble(block.values()[i]) < min) {
				min = Double.parseDouble(block.values()[i]);
				minIndex = i;
			}
			} catch (NumberFormatException e) {
				return nb;
			}
		} 
		nb.add(block.fields()[minIndex], Double.toString(min));
		return nb;
	}

}
