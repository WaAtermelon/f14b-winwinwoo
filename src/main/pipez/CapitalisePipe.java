package pipez;

import pipez.core.Block;
import pipez.core.Pipe;
import pipez.core.SimpleBlock;

import static pipez.core.SpecialBlocks.*;

/**
 * Capitalises text fields.
 * 
 * @author slin3916
 *
 */
public class CapitalisePipe implements Pipe {

	@Override
	public String getName() {
		return "Capitalise";
	}
	
	private CapitalisePipe() {
	}

	public static CapitalisePipe create() {
		return new CapitalisePipe();
	}

	@Override
	public Block transform(Block block) {
		SimpleBlock nb = new SimpleBlock();
		for (int i = 0; i < block.values().length; i++) {
			String uppercase = block.values()[i].toUpperCase();
			nb.add(uppercase);
			}
		return nb;
	}

}
