package pipez;


import pipez.core.Block;
import pipez.core.Pipe;
import pipez.core.SimpleBlock;

/**
 * Selects the n-th field of each block
 * e.g. select the n-th column of a CSV file.
 *
 *  n=1 corresponds to the first column.
 *  n=-1 corresponds to the last column.
 *
 * Also allows multiple fields to be selected.
 *
 * @author whwong
 *
 */
public class NFieldPipe implements Pipe {

	public static NFieldPipe create(int n) {
		return new NFieldPipe(n);
	}

	public static NFieldPipe create(int... ns) {
		return new NFieldPipe(ns);
	}

	int[] ns;
	private NFieldPipe(int... ns) {
		this.ns = ns;
		// store the original value. do not convert into index. 
		// because we do not know how to convert negative index.
		// we can only convert negative index during transform, 
		// when we know the size of the block.
	}


	@Override
	public String getName() {
		return "n-th field pipe";
	}

	@Override
	public Block transform(Block block) {
		SimpleBlock newBlock = new SimpleBlock();

		String[] fields = block.fields();
		for(int n: ns) {
			if (n < 0) {
				// convert negative index to last n-th item
				n = fields.length + n;
			}
			else if (n > 0) {
				// convert 1-based index to 0
				n = n - 1;
			}
			else {
				// skip
				continue;
			}
			
			// check we are in bound.
			if (n >=0 && n < fields.length) {
				// debug what is happening during transform.
				// System.out.println("fields[" + n + "] = " + block.value(fields[n]));
				newBlock.add(fields[n], block.value(fields[n]));
			}
		}
		return newBlock;
	}

}