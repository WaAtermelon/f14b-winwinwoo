package pipez;

import pipez.core.Block;
import pipez.core.Pipe;
import pipez.core.SimpleBlock;

import static pipez.core.SpecialBlocks.*;

/**
 * Returns only Blocks which have at least one value which has the given string.
 * e.g. for a given CSV file, only lines which contain the given string are passed through.
 * 
 * Also includes an inverse-mode, which does not pass through Blocks that have at least
 * one value which has the given string.
 * 
 * @author whwong
 *
 */
public class ValueMatchPipe implements Pipe {

	@Override
	public String getName() {
		return "Value Match";
	}

	private String toMatch = null;
	private boolean inverse = false, ignoreCase = false;
	
	private ValueMatchPipe(String toMatch, boolean inverse, boolean ignoreCase) {
		this.toMatch = toMatch;
		this.inverse = inverse;
		this.ignoreCase = ignoreCase;
	}
	
	public static ValueMatchPipe create(String toMatch) {
		return new ValueMatchPipe(toMatch, false, false);
	}
	
	public static ValueMatchPipe createIgnoreCase(String toMatch) {
		return new ValueMatchPipe(toMatch, false, true);
	}
	
	public static ValueMatchPipe createInverse(String toMatch) {
		return new ValueMatchPipe(toMatch, true, false);
	}
	
	public static ValueMatchPipe createInverseIgnoreCase(String toMatch) {
		return new ValueMatchPipe(toMatch, true, true);
	}
	
	@Override
	public Block transform(Block block) {
		
		if (inverse == false && ignoreCase == false) {
			for (String v : block.values()) {
				if (v.equals(toMatch)) {
					return block;
				}
		} 
		} else if (inverse == false && ignoreCase == true) {
		for (String v : block.values()) {
			if (v.equalsIgnoreCase(toMatch)) {
				return block;
			}
		}
		} else {
			//Test if any of the fields matches with the input string. If so, return the empty block.
			for (String v : block.values()) {
				if (ignoreCase == false && v.equals(toMatch)) {
					return SKIP_BLOCK;
				} else if (ignoreCase == true && v.equalsIgnoreCase(toMatch)) {
					return SKIP_BLOCK;
				}
			}
			//Return the original block if the input string does not match with any fields. No need to check for ignoreCase because all instances where the input string matches the field's value but not its case will have already been accounted for in the previous section.
				for (String v : block.values()) {
					if (v.equals(toMatch) != true) {
						return block;
					}
			}
			
			
		}
		return SKIP_BLOCK;
	}

}
