package pipez;

import pipez.core.Block;
import pipez.core.Pipe;
import pipez.core.SimpleBlock;
import java.util.Arrays;

import static pipez.core.SpecialBlocks.*;

/**
 * Sorts the fields based on numerical and alphabetical order
 * 
 * @author slin3916
 *
 */
public class SortFieldsPipe implements Pipe {

	@Override
	public String getName() {
		return "Sort Fields";
	}
	
	private SortFieldsPipe() {
	}

	public static SortFieldsPipe create() {
		return new SortFieldsPipe();
	}

	@Override
	public Block transform(Block block) {
		String[] array = new String[block.values().length];
		SimpleBlock nb = new SimpleBlock();
		for (int i = 0; i < block.values().length; i++) {
			//Copy each field to the array
			array[i] = block.values()[i];
			}
		//Sort the array. String.CASE_INSENSITIVE_ORDER will ignore the case of letters
		//otherwise XYZ will be placed in front of abc
		Arrays.sort(array, String.CASE_INSENSITIVE_ORDER); 
		for (int i = 0; i < array.length; i++) {
			//Add each array item to the block
			nb.add(array[i]);
		}
		return nb;
	}

}
